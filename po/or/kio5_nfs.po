# translation of kio_nfs.po to Oriya
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Manoj Kumar Giri <mgiri@redhat.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: kio_nfs\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-05-06 00:45+0000\n"
"PO-Revision-Date: 2009-01-02 11:48+0530\n"
"Last-Translator: Manoj Kumar Giri <mgiri@redhat.com>\n"
"Language-Team: Oriya <oriya-it@googlegroups.com>\n"
"Language: or\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"\n"
"\n"

#: kio_nfs.cpp:156
#, kde-format
msgid "Cannot find an NFS version that host '%1' supports"
msgstr ""

#: kio_nfs.cpp:336
#, kde-format
msgid "The NFS protocol requires a server host name."
msgstr ""

#: kio_nfs.cpp:369
#, kde-format
msgid "Failed to initialise protocol"
msgstr ""

#: kio_nfs.cpp:852
#, kde-format
msgid "RPC error %1, %2"
msgstr ""

#: kio_nfs.cpp:902
#, kde-format
msgid "Filename too long"
msgstr ""

#: kio_nfs.cpp:909
#, kde-format
msgid "Disk quota exceeded"
msgstr ""

#: kio_nfs.cpp:915
#, kde-format
msgid "NFS error %1, %2"
msgstr ""

#: nfsv2.cpp:394 nfsv2.cpp:446 nfsv3.cpp:426 nfsv3.cpp:570
#, kde-format
msgid "Unknown target"
msgstr ""
